This project provides a recommender system build with PySpark using the Collaborative Filtering (CF)
provided by the [MLLIB](https://spark.apache.org/docs/latest/ml-collaborative-filtering.html) library in Spark.

The CF approach uses the Alternative Least Squares optimization.

We have used the [MovieLens](https://movielens.org/) dataset for this experiment: the ml-latest-small. It contains 100004 ratings and 1296 tag applications across 9125 movies.

This application also provides a Flask Application and Swagger specification to turn it into an API.

References
- https://spark.apache.org/docs/latest/ml-collaborative-filtering.html



