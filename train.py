from pyspark.mllib.recommendation import ALS
from pyspark import SparkContext, SparkConf
import math


"""
training recommender using Alternating Least Squares with latent factors
recommandation as an optimization problem
ALS fixes the param user vec X while solving the other (item vec Y)
trying to find the convergence of this serie
"""

# define spark context
def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("movie_recommendation-server")
    sc = SparkContext(conf=conf)

    return sc


# Load and parse the data
def parse_data(path):
    small_ratings_raw_data = sc.textFile(path)
    small_ratings_raw_data_header = small_ratings_raw_data.take(1)[0]

    small_ratings_data = small_ratings_raw_data.filter(lambda line: line!=small_ratings_raw_data_header)\
        .map(lambda line: line.split(",")).map(lambda tokens: (tokens[0],tokens[1],tokens[2])).cache()

    return  small_ratings_data


# Build the recommendation model using Alternating Least Squares
def train_test():
    # init dataset path and parse dataset
    datasets_path = '/Users/knebhi/workspace/git/recommender-system-pyspark/datasets/ml-latest-small/ratings.csv'
    data = parse_data(datasets_path)

    ### training parameters to try
    #  maximum number of iterations to run
    iterations = 10
    # regularization parameter in ALS - cost term
    regularization_parameter = 0.1
    # latent factors in the model
    ranks = [4, 8, 12, 16]

    # init root-mean-square errors table for 4 ranks
    errors = [0, 0, 0, 0]
    err = 0

    # split dataset intro training, validation and test
    training_RDD, validation_RDD, test_RDD = data.randomSplit([6, 2, 2], seed=0L)
    validation_for_predict_RDD = validation_RDD.map(lambda x: (x[0], x[1]))
    test_for_predict_RDD = test_RDD.map(lambda x: (x[0], x[1]))

    # init var to define best rank and min error
    min_error = float('inf')
    best_rank = -1

    # try each model using the different params
    for rank in ranks:
        # init ALS model
        model = ALS.train(training_RDD, rank, iterations=iterations,
                          lambda_=regularization_parameter)
        predictions = model.predictAll(validation_for_predict_RDD).map(lambda r: ((r[0], r[1]), r[2]))
        rates_and_preds = validation_RDD.map(lambda r: ((int(r[0]), int(r[1])), float(r[2]))).join(predictions)
        # calculate RMSE WITH sqrt AVG(square root of X - Y Exponent 2)
        # standard deviation predicted by the model and the values observed
        error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean())
        errors[err] = error
        err += 1
        print 'For rank %s the RMSE is %s' % (rank, error)
        if error < min_error:
            min_error = error
            best_rank = rank

    print 'The best model was trained with rank %s' % best_rank

    # evaluate best model on testing data
    model = ALS.train(training_RDD, best_rank, iterations=iterations,
                      lambda_=regularization_parameter)
    predictions = model.predictAll(test_for_predict_RDD).map(lambda r: ((r[0], r[1]), r[2]))
    rates_and_preds = test_RDD.map(lambda r: ((int(r[0]), int(r[1])), float(r[2]))).join(predictions)
    error = math.sqrt(rates_and_preds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean())

    print 'For testing data the RMSE is %s' % (error)


if __name__ == '__main__':
    sc = init_spark_context()
    train_test()